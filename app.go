package baixing

import (
	"fmt"
	_ "gitee.com/banyanhouse/distill-db-tx/core/mysql_tx"
	infra "gitee.com/banyanhouse/distill-infra"
	"gitee.com/banyanhouse/distill-infra/api"
	"gitee.com/banyanhouse/distill-infra/base"
	"gitee.com/banyanhouse/distill-infra/db"
	"gitee.com/banyanhouse/distill-infra/hook"
	"gitee.com/banyanhouse/distill-infra/log"
	"gitee.com/banyanhouse/distill-infra/register"
	"gitee.com/banyanhouse/distill-infra/validator"
	"gitee.com/banyanhouse/distill-infra/web"
)

func init() {
	fmt.Println("start app...")
	infra.Register(&base.TomlPropsStarter{})
	infra.Register(&db.XormDatabaseStarter{})
	infra.Register(&log.LoggerStarter{}) // 如果需要输出日志到文件中，则可以打开这里，去初始化文件日志
	infra.Register(&validator.ValidatorStarter{})
	infra.Register(&web.IrisServerStarter{})
	infra.Register(&api.WebApiStarter{})
	infra.Register(&register.Etcd3Starter{})
	infra.Register(&hook.HookStarter{})
	fmt.Println("load end ...")
}
