package main

import (
	_ "gitee.com/banyanhouse/distill-db-tx"
	conf "gitee.com/banyanhouse/distill-infra/config"
	"log"
	"os"

	//_ "gitee.com/Hu-Lyndon/distill-resk"
	infra "gitee.com/banyanhouse/distill-infra"

	"gitee.com/banyanhouse/distill-infra/utils"
)

var (
	Version string = "0.0.1"

	//h bool

	//v bool

	configFilePath string

	//modelFilePath string
	//wwwDir        string
	//swaggerDir    string
	//port          int
)

func main() {
	// 获取程序运行，所需配置文件的路径

	if configFilePath == "" {
		configFilePath = utils.GetCurrentPath("./config/") + "/config.toml"
	}

	extensionMap := make(map[string]interface{})
	extensionMap["gatewayConfig"] = configFilePath

	appConfig := &conf.TomlConfig{}

	//utils.ConfigFromFile(configFilePath, config)
	configFiles := []string{configFilePath, utils.GetCurrentPath("../config/") + "/config.toml"}
	utils.ConfigFromSlice(configFiles, appConfig)

	app := infra.New(appConfig)
	app.Start()
	// 之前是因为没有阻塞的starter，有了Iris就不用下面的了
	//c := make(chan int)
	//<- c
}

func ErrHandler(err error) {
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(-1)
	}
}
