package mysql_tx

import (
	"fmt"
	"gitee.com/banyanhouse/distill-infra/db"
	"github.com/sirupsen/logrus"
)

type DaoOrders struct {
	runner *db.TxRunner
}

func (do *DaoOrders) InsertOne(o *TTxOrders) (id int64, err error) {
	return do.runner.InsertOne(o)
}

func (do *DaoOrders) GetOne(name string) *TTxOrders {
	tTxOrders := new(TTxOrders)
	//do.runner.Table(tTxOrders.TableName())
	ok, err := do.runner.Where(" name = ? ", name).Desc("id").Limit(1).Get(tTxOrders)
	if err != nil {
		logrus.Error(err)
		return nil
	}
	if !ok {
		return nil
	}
	return tTxOrders
}

func (do *DaoOrders) DeleteAll() (effect int64, err error) {
	tTxOrders := new(TTxOrders)
	//effect, err = do.runner.Where(" 1 = 1 ").Delete(tTxOrders)
	//return

	sql := fmt.Sprintf("truncate table %s", tTxOrders.TableName())
	res, err := do.runner.SQL(sql).Execute()
	if err != nil {
		return 0, err
	}
	effect, err = res.RowsAffected()
	return
}
