package mysql_tx

import (
	"fmt"
	"gitee.com/banyanhouse/distill-infra/db"
	"github.com/sirupsen/logrus"
)

type DaoOrdersDetails struct {
	runner *db.TxRunner
}

func (do *DaoOrdersDetails) Insert(ods []*TTxOrdersDetails) (count int64, err error) {
	return do.runner.InsertMulti(ods)
}

func (do *DaoOrdersDetails) GetMultiByOrderId(orderId int) []*TTxOrdersDetails {

	txOrdersDetailsList := make([]*TTxOrdersDetails, 0)

	err := do.runner.Where(" order_id = ? ", orderId).Find(&txOrdersDetailsList)

	if err != nil {
		logrus.Error(err)
		return nil
	}

	return txOrdersDetailsList
}

func (do *DaoOrdersDetails) DeleteAll() (effect int64, err error) {
	tTxOrdersDetails := new(TTxOrdersDetails)
	//effect, err = do.runner.Where(" 1 = 1 ").Delete(tTxOrdersDetails)
	//return

	sql := fmt.Sprintf("truncate table %s", tTxOrdersDetails.TableName())
	res, err := do.runner.SQL(sql).Execute()
	if err != nil {
		return 0, err
	}
	effect, err = res.RowsAffected()
	return
}
