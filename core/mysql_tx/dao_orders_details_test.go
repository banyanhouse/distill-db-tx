package mysql_tx

import (
	"fmt"
	_ "gitee.com/banyanhouse/distill-db-tx/testx"
	"gitee.com/banyanhouse/distill-infra/db"
	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestDaoOrdersDetailsControl(t *testing.T)  {
	t.Run("清除订单行记录", testDaoOrdersDetails_Clean)
	t.Run("插入订单行记录", testDaoOrdersDetails_Insert)
	t.Run("获取订单行记录", testDaoOrdersDetails_GetMultiByOrderId)
	t.Run("清除订单行记录", testDaoOrdersDetails_Clean)
}


func testDaoOrdersDetails_Insert(t *testing.T) {
	err := db.Tx(func(runner *db.TxRunner) error {

		daoOrdersDetails := new(DaoOrdersDetails)
		daoOrdersDetails.runner = runner

		Convey("插入订单行测试", t, func() {

			_, err := daoOrdersDetails.DeleteAll()
			So(err, ShouldBeNil)

			ordersDetails := []*TTxOrdersDetails{
				{
					//Id:        0,
					OrderId:   1,
					GoodsId:   1001,
					GoodsName: "洗发水",
				},
				{
					//Id:        1,
					OrderId:   1,
					GoodsId:   1002,
					GoodsName: "风油精",
				},
				{
					//Id:        2,
					OrderId:   1,
					GoodsId:   1003,
					GoodsName: "洁面乳",
				},
			}

			count, err := daoOrdersDetails.Insert(ordersDetails)
			So(err, ShouldBeNil)
			fmt.Println("count", count)
			So(count, ShouldEqual, 3)
		})
		return nil
	})

	if err != nil {
		log.Error(err)
	}
}

func testDaoOrdersDetails_GetMultiByOrderId(t *testing.T) {
	err := db.Tx(func(runner *db.TxRunner) error {
		daoOrdersDetails := new(DaoOrdersDetails)
		daoOrdersDetails.runner = runner
		Convey("通过订单id获取订单行记录", t, func() {
			txOrdersDetailsList := daoOrdersDetails.GetMultiByOrderId(1)
			So(txOrdersDetailsList, ShouldNotBeNil)
			for _, details := range txOrdersDetailsList {
				fmt.Println(details)
			}
		})
		return nil
	})

	if err != nil {
		t.Error(err)
	}
}


func testDaoOrdersDetails_Clean(t *testing.T) {
	err := db.Tx(func(runner *db.TxRunner) error {

		daoOrdersDetails := new(DaoOrdersDetails)
		daoOrdersDetails.runner = runner

		Convey("清空初始化订单行表", t, func() {

			_, err := daoOrdersDetails.DeleteAll()
			So(err, ShouldBeNil)
		})
		return nil
	})

	if err != nil {
		log.Error(err)
	}
}