package mysql_tx

import (
	"fmt"
	_ "gitee.com/banyanhouse/distill-db-tx/testx"
	"gitee.com/banyanhouse/distill-infra/db"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)


func TestDaoOrdersControl(t *testing.T)  {
	t.Run("清除表记录", testDaoOrders_Clean)
	t.Run("插入一条订单记录", testDaoOrders_InsertOne)
	t.Run("获取一条订单记录", testDaoOrders_GetOne)
	t.Run("清除表记录", testDaoOrders_Clean)
}


func testDaoOrders_GetOne(t *testing.T) {

	err := db.Tx(func(runner *db.TxRunner) error {
		daoOrders := new(DaoOrders)
		daoOrders.runner = runner
		Convey("插入订单", t, func() {
			txOrders := &TTxOrders{
				Name: "我的订单",
			}
			tTxOrders := daoOrders.GetOne(txOrders.Name)
			fmt.Printf("GetOne: id -> %d, name -> %s\n", tTxOrders.Id, tTxOrders.Name)
			So(tTxOrders, ShouldNotBeNil)
		})
		return nil
	})
	if err != nil {
		logrus.Error(err)
	}

}


func testDaoOrders_InsertOne(t *testing.T) {

	err := db.Tx(func(runner *db.TxRunner) error {
		daoOrders := new(DaoOrders)
		daoOrders.runner = runner
		Convey("插入订单", t, func() {

			_, err := daoOrders.DeleteAll()
			So(err, ShouldBeNil)

			txOrders := &TTxOrders{
				Name: "我的订单",
			}
			one, err := daoOrders.InsertOne(txOrders)
			So(err, ShouldBeNil)
			fmt.Println(one)
			So(one, ShouldEqual, 1)

			tTxOrders := daoOrders.GetOne(txOrders.Name)
			fmt.Printf("GetOne: id -> %d, name -> %s\n", tTxOrders.Id, tTxOrders.Name)
			So(tTxOrders, ShouldNotBeNil)
		})
		return nil //errors.New("aaaaa")  // TX中返回错误，事务回滚，反之提交。
	})
	if err != nil {
		logrus.Error(err)
	}
}


func testDaoOrders_Clean(t *testing.T) {

	err := db.Tx(func(runner *db.TxRunner) error {
		daoOrders := new(DaoOrders)
		daoOrders.runner = runner
		Convey("清除表记录", t, func() {

			_, err := daoOrders.DeleteAll()
			So(err, ShouldBeNil)
		})
		return nil //errors.New("aaaaa")  // TX中返回错误，事务回滚，反之提交。
	})
	if err != nil {
		logrus.Error(err)
	}
}


