package mysql_tx

import (
	"errors"
	"gitee.com/banyanhouse/distill-db-tx/services"
	"gitee.com/banyanhouse/distill-infra/db"
)

type ordersDomain struct {
	tTxOrders            TTxOrders
	tTxOrdersDetailsList []*TTxOrdersDetails
}

func NewOrdersDomain() *ordersDomain {
	return new(ordersDomain)
}

func (domain *ordersDomain) CreateOrders(dtoOrders services.DtoOrders) (*services.DtoOrders, error) {
	domain.tTxOrders = TTxOrders{}
	domain.tTxOrders.FromDTO(&dtoOrders)

	daoOrders := DaoOrders{}
	daoOrdersDetails := DaoOrdersDetails{}
	var rdto *services.DtoOrders
	err := db.Tx(func(runner *db.TxRunner) error {
		daoOrders.runner = runner
		daoOrdersDetails.runner = runner

		count, err := daoOrders.InsertOne(&domain.tTxOrders)
		if err != nil {
			return err
		}
		if count < 0 {
			return errors.New("写入订单失败")
		}

		txOrders := daoOrders.GetOne(dtoOrders.Name)
		if txOrders == nil {
			return errors.New("获取记录的订单失败")
		}
		// 获得真正的订单编号
		domain.tTxOrders = *txOrders

		// 将订单编号写入明细
		for _, detail := range dtoOrders.Detail {
			ordersDetails := new(TTxOrdersDetails)
			ordersDetails.FromDTO(&domain.tTxOrders, detail)
			domain.tTxOrdersDetailsList = append(domain.tTxOrdersDetailsList, ordersDetails)
		}

		count, err = daoOrdersDetails.Insert(domain.tTxOrdersDetailsList)
		if err != nil {
			return err
		}
		if count < 0 {
			return errors.New("写入订单明细失败")
		}

		return nil
	})

	txOrdersDetailsList := daoOrdersDetails.GetMultiByOrderId(domain.tTxOrders.Id)
	if txOrdersDetailsList == nil {
		return nil, errors.New("获取记录的订单明细失败")
	}
	domain.tTxOrdersDetailsList = txOrdersDetailsList
	if err != nil {
		return nil, err
	}

	rdto = domain.tTxOrders.ToWithDetailDTO(domain.tTxOrdersDetailsList)

	return rdto, nil
}