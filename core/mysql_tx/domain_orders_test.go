package mysql_tx

import (
	"encoding/json"
	"fmt"
	"gitee.com/banyanhouse/distill-db-tx/services"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func testOrdersDomain_CreateOrders(t *testing.T) {
	dtoOrders := services.DtoOrders{
		Name:   "测试订单",
		Detail: []*services.DtoOrdersDetail{
			{
				GoodsId:   1001,
				GoodsName: "洗发水",
			},
			{
				GoodsId:   1002,
				GoodsName: "风油精",
			},
			{
				GoodsId:   1003,
				GoodsName: "洁面乳",
			},
		},
	}

	domain := NewOrdersDomain()

	Convey("测试整体订单插入主副表", t, func() {
		createdOrders, err := domain.CreateOrders(dtoOrders)
		So(err, ShouldBeNil)
		So(createdOrders, ShouldNotBeNil)
		createdOrdersJson, err := json.Marshal(createdOrders)
		So(err, ShouldBeNil)
		So(createdOrdersJson, ShouldNotBeNil)
		fmt.Println(string(createdOrdersJson))
	})
}

func TestDomainOrdersControl(t *testing.T) {
	t.Run("清除订单表", testDaoOrders_Clean)
	t.Run("清除订单行表", testDaoOrdersDetails_Clean)
	t.Run("插入创建订单、订单行表记录", testOrdersDomain_CreateOrders)
	t.Run("清除订单表", testDaoOrders_Clean)
	t.Run("清除订单行表", testDaoOrdersDetails_Clean)
}
