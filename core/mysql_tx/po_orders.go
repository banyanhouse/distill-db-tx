package mysql_tx

import "gitee.com/banyanhouse/distill-db-tx/services"

type TTxOrders struct {
	Id   int    `json:"id,omitempty" xorm:"not null pk autoincr SMALLINT(6)"`
	Name string `json:"name" xorm:"VARCHAR(100)"`
}


func (to TTxOrders) TableName() string {
	return "t_tx_orders"
}

func (to *TTxOrders) ToDTO() *services.DtoOrders {
	dto := new(services.DtoOrders)
	dto.Id = to.Id
	dto.Name = to.Name
	return dto
}

func (to *TTxOrders) ToWithDetailDTO(tod []*TTxOrdersDetails) *services.DtoOrders {
	dto := new(services.DtoOrders)
	dto.Id = to.Id
	dto.Name = to.Name
	for _, txOrdersDetails := range tod {
		dto.Detail = append(dto.Detail, txOrdersDetails.ToDTO())
	}
	return dto
}

func (to *TTxOrders) FromDTO(dto *services.DtoOrders) {
	to.Id = dto.Id
	to.Name = dto.Name
}