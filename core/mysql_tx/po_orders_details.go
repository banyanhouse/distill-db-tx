package mysql_tx

import "gitee.com/banyanhouse/distill-db-tx/services"

type TTxOrdersDetails struct {
	Id        int    `json:"id,omitempty" xorm:"not null pk autoincr SMALLINT(6)"`
	OrderId   int    `json:"order_id" xorm:"not null SMALLINT(6)"`
	GoodsId   int    `json:"goods_id" xorm:"not null SMALLINT(6)"`
	GoodsName string `json:"goods_name" xorm:"VARCHAR(100)"`
}

func (tod *TTxOrdersDetails) TableName() string {
	return "t_tx_orders_details"
}

func (tod *TTxOrdersDetails) ToDTO() *services.DtoOrdersDetail {
	dto := new(services.DtoOrdersDetail)
	dto.Id = tod.Id
	dto.GoodsId = tod.GoodsId
	dto.GoodsName = tod.GoodsName
	return dto
}

func (tod *TTxOrdersDetails) FromDTO(tTxOrders *TTxOrders, dtoOrderDetail *services.DtoOrdersDetail) {
	tod.Id = dtoOrderDetail.Id
	tod.OrderId = tTxOrders.Id
	tod.GoodsId = dtoOrderDetail.GoodsId
	tod.GoodsName = dtoOrderDetail.GoodsName
}