drop table t_tx_orders;
CREATE TABLE t_tx_orders
    (

        id SMALLINT NOT NULL auto_increment,
        name VARCHAR(100),
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_general_ci COMMENT='t_tx_orders';

 drop table t_tx_orders_details;
 CREATE TABLE t_tx_orders_details
    (
        id SMALLINT NOT NULL auto_increment,
        order_id SMALLINT NOT NULL,
        goods_id SMALLINT NOT NULL,
        goods_name VARCHAR(100),
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_general_ci COMMENT='t_tx_orders_details';