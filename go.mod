module gitee.com/banyanhouse/distill-db-tx

go 1.13

require (
	gitee.com/banyanhouse/distill-infra v0.0.16
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v1.6.4
	github.com/xormplus/xorm v0.0.0-20200514184607-0f37421d8714
)

replace (
	cloud.google.com/go => github.com/googleapis/google-cloud-go v0.41.0
	go.etcd.io/etcd => github.com/etcd-io/etcd v3.3.13+incompatible
	go.uber.org/zap => github.com/uber-go/zap v1.10.0
	golang.org/x/crypto => github.com/golang/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/exp => github.com/golang/exp v0.0.0-20190731235908-ec7cb31e5a56
	golang.org/x/image => github.com/golang/image v0.0.0-20190802002840-cff245a6509b
	golang.org/x/lint => github.com/golang/lint v0.0.0-20190409202823-959b441ac422
	golang.org/x/mobile => github.com/golang/mobile v0.0.0-20190814143026-e8b3e6111d02
	golang.org/x/mod => github.com/golang/mod v0.1.0
	golang.org/x/net => github.com/golang/net v0.0.0-20190813141303-74dc4d7220e7
	golang.org/x/oauth2 => github.com/golang/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sync => github.com/golang/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/sys => github.com/golang/sys v0.0.0-20190813064441-fde4db37ae7a
	golang.org/x/text => github.com/golang/text v0.3.2
	golang.org/x/time => github.com/golang/time v0.0.0-20190308202827-9d24e82272b4
	golang.org/x/tools => github.com/golang/tools v0.0.0-20190816200558-6889da9d5479
	golang.org/x/xerrors => github.com/golang/xerrors v0.0.0-20190717185122-a985d3407aa7
	google.golang.org/api => github.com/googleapis/google-api-go-client v0.7.0
	google.golang.org/appengine => github.com/golang/appengine v1.6.1
	google.golang.org/genproto => github.com/google/go-genproto v0.0.0-20190817000702-55e96fffbd48
	google.golang.org/grpc => github.com/grpc/grpc-go v1.22.0
	gopkg.in/flyaways/pool.v1 => github.com/flyaways/pool v1.0.1
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 => github.com/gemnasium/logrus-airbrake-hook v2.1.2+incompatible
	honnef.co/go/tools => github.com/dominikh/go-tools v0.0.0-20190614002413-cb51c254f01b // indirect
)
