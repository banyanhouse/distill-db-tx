package services

import "gitee.com/banyanhouse/distill-infra/utils"

var IOrdersService OrdersService

func GetOrdersService() OrdersService {
	utils.Check(IOrdersService)
	return IOrdersService
}

type OrdersService interface {
	CreateOrders()
}

type DtoOrders struct {
	Id int `validate:"required" json:"id"`
	Name string `validate:"required" json:"name"`
	Detail []*DtoOrdersDetail `validate:"required" json:"ordersDetail"`
}

type DtoOrdersDetail struct {
	Id int `validate:"required" json:"id"`
	GoodsId int `validate:"required" json:"goods_id"`
	GoodsName string `validate:"required" json:"goods_name"`
}