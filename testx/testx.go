package testx

import (
	infra "gitee.com/banyanhouse/distill-infra"
	"gitee.com/banyanhouse/distill-infra/base"
	conf "gitee.com/banyanhouse/distill-infra/config"
	"gitee.com/banyanhouse/distill-infra/db"
	"gitee.com/banyanhouse/distill-infra/utils"
	"gitee.com/banyanhouse/distill-infra/validator"
)

var (
	configFilePath string
)

func init() {
	if configFilePath == "" {
		configFilePath = utils.GetCurrentPath("../../config/") + "/config.toml"
	}

	extensionMap := make(map[string]interface{})
	extensionMap["gatewayConfig"] = configFilePath

	appConfig := &conf.TomlConfig{}

	//utils.ConfigFromFile(configFilePath, config)
	configFiles := []string{configFilePath, utils.GetCurrentPath("../config/") + "/config.toml"}
	utils.ConfigFromSlice(configFiles, appConfig)

	infra.Register(&base.TomlPropsStarter{})
	infra.Register(&db.XormDatabaseStarter{})
	infra.Register(&validator.ValidatorStarter{})

	app := infra.New(appConfig)
	app.Start()
}
